package main

import (
	"fmt"
	hello "hello/hello_world"
)

var s string = "long"

func main() {
	//hello.Hello()
	fmt.Println(hello.HelloWorld())
	s = `"short"  - new 

line`
	fmt.Println(s)

	n := 5
	m := "hello"
	fmt.Printf("%T\n", n)
	fmt.Printf("%T\n", m)

}
