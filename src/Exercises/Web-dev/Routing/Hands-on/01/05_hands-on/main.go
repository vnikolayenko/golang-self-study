package main

import (
	"html/template"
	"net/http"
)

var tpl *template.Template

func dog(res http.ResponseWriter, req *http.Request) {
	tpl.Execute(res, "dog")
	//fmt.Fprint(res, "dog Response")
}
func me(res http.ResponseWriter, req *http.Request) {
	tpl.Execute(res, "Nikolaienko Vladyslav")
	//fmt.Fprint(res, "Nikolaienko Vladyslav")
}
func slash(res http.ResponseWriter, req *http.Request) {
	tpl.Execute(res, "Hello World!")
	//fmt.Fprint(res, "Hello World!")
}

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}
func main() {
	http.Handle("/", http.HandlerFunc(slash))
	http.Handle("/dog/", http.HandlerFunc(dog))
	http.Handle("/me", http.HandlerFunc(me))
	http.ListenAndServe(":8080", nil)
}
