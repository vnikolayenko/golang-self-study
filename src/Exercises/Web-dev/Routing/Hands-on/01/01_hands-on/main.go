package main

import (
	"fmt"
	"net/http"
)

func dog(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "dog Response")
}
func me(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "Nikolaienko Vladyslav")
}
func slash(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "Hello World!")
}

func main() {
	http.HandleFunc("/", slash)
	http.HandleFunc("/dog/", dog)
	http.HandleFunc("/me", me)
	http.ListenAndServe(":8080", nil)
}
