package main

import (
	"io"
	"log"
	"net"
)

func main() {
	listen, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Can't listem tcp localhost:8080. Err: %v", err)
	}
	defer listen.Close()
	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Fatalf("Can't accept traffic. Err: %v", err)
			continue
		}
		io.WriteString(conn, "Connection established")
		conn.Close()
	}

}
