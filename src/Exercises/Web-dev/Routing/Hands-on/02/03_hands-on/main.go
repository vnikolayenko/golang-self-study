package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	listen, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Can't listem tcp localhost:8080. Err: %v", err)
	}
	defer listen.Close()
	for {
		conn, err := listen.Accept()
		if err != nil {
			log.Fatalf("Can't accept traffic. Err: %v", err)
			continue
		}

		go request(conn)

		io.WriteString(conn, "Connection established\n")
	}

}

func request(conn net.Conn) {
	scanner := bufio.NewScanner(conn)
	defer conn.Close()
	for scanner.Scan() {
		text := scanner.Text()
		fmt.Println(text)
		if text == "" {
			// when ln is empty, header is done
			fmt.Println("THIS IS THE END OF THE HTTP REQUEST HEADERS")
			break
		}

	}
}
