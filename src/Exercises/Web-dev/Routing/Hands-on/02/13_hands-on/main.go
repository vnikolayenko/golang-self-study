package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
)

func main() {
	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln(err)
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		serve(conn)
	}
}

func serve(c net.Conn) {
	method := ""
	path := ""
	defer c.Close()
	scanner := bufio.NewScanner(c)
	i := 0
	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(ln, i)
		if i == 0 {
			method = strings.Fields(ln)[0]
			path = strings.Fields(ln)[1]
			fmt.Println(method, path)
		}
		if ln == "" {
			// when ln is empty, header is done
			fmt.Println("THIS IS THE END OF THE HTTP REQUEST HEADERS")
			break
		}
		i++
	}
	//body := "Method: "
	//body += method
	//body += "\n"
	//body += "Path: "
	//body += path
	//body += "\n"

	//io.WriteString(c, "HTTP/1.1 200 OK\r\n")
	//fmt.Fprintf(c, "Content-Length: %d\r\n", len(body))
	//fmt.Fprint(c, "Content-Type: text/plain\r\n")
	//io.WriteString(c, "\r\n")
	//io.WriteString(c, body)

	//body := `
	//	<!DOCTYPE html>
	//	<html lang="en">
	//	<head>
	//		<meta charset="UTF-8">
	//		<title>Code Gangsta</title>
	//	</head>
	//	<body>
	//		<h1>"HOLY COW THIS IS LOW LEVEL"</h1>
	//	</body>
	//	</html>
	//`
	//
	//io.WriteString(c, "HTTP/1.1 200 OK\r\n")
	//fmt.Fprintf(c, "Content-Length: %d\r\n", len(body))
	//fmt.Fprint(c, "Content-Type: text/html\r\n")
	//io.WriteString(c, "\r\n")
	//io.WriteString(c, body)

	switch {
	case method == "GET" && path == "/":
		body := `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Code Gangsta</title>
		</head>
		<body>
			<h1>"Method GET. Path /"</h1>
		</body>
		</html>
	`

		io.WriteString(c, "HTTP/1.1 200 OK\r\n")
		fmt.Fprintf(c, "Content-Length: %d\r\n", len(body))
		fmt.Fprint(c, "Content-Type: text/html\r\n")
		io.WriteString(c, "\r\n")
		io.WriteString(c, body)
	case method == "GET" && path == "/apply":
		body := `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Code Gangsta</title>
		</head>
		<body>
			<h1>"Method GET. Path /apply"</h1>
		</body>
		</html>
	`

		io.WriteString(c, "HTTP/1.1 200 OK\r\n")
		fmt.Fprintf(c, "Content-Length: %d\r\n", len(body))
		fmt.Fprint(c, "Content-Type: text/html\r\n")
		io.WriteString(c, "\r\n")
		io.WriteString(c, body)
	case method == "POST" && path == "/apply":
		body := `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Code Gangsta</title>
		</head>
		<body>
			<h1>"Method POST. Path /apply"</h1>
		</body>
		</html>
	`

		io.WriteString(c, "HTTP/1.1 200 OK\r\n")
		fmt.Fprintf(c, "Content-Length: %d\r\n", len(body))
		fmt.Fprint(c, "Content-Type: text/html\r\n")
		io.WriteString(c, "\r\n")
		io.WriteString(c, body)

	}
}
