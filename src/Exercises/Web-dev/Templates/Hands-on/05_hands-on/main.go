package main

import (
	"html/template"
	"log"
	"os"
)

type dishes map[string]int

type menu struct {
	Time   string
	Dishes dishes
}

type restaurant struct {
	Name string
	Menu []menu
}

var Nikolaienko = restaurant{
	Name: "Nikolaienko",
	Menu: []menu{
		{
			Time: "Dinner",
			Dishes: dishes{
				"soap":   1,
				"borsch": 3,
			},
		},
		{
			Time: "Breakfest",
			Dishes: dishes{
				"eggs":  1,
				"omlet": 3,
			},
		},
	},
}

var Vodopian = restaurant{
	Name: "Vodopian",
	Menu: []menu{
		{
			Time: "Dinner",
			Dishes: dishes{
				"soap-2":   2,
				"borsch-2": 4,
			},
		},
		{
			Time: "Breakfest",
			Dishes: dishes{
				"eggs-2":  5,
				"omlet-2": 6,
			},
		},
	},
}
var restaurants = []restaurant{Nikolaienko, Vodopian}

var tpl *template.Template

var name = "tpl.gohtml"

func init() {
	tpl = template.Must(template.ParseFiles(name))
	//tpl = template.Must(template.New("test").Parse("test"))
}

func main() {
	file, err := os.Create("index.html")
	defer file.Close()
	if err != nil {
		log.Fatalf("Couldn't parse file. %v", err)
	}
	err = tpl.ExecuteTemplate(file, name, restaurants)
	if err != nil {
		log.Fatalf("Template execution error occured:", err)
	}

}
