package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

var datanew [][]string

func init() {

}

var date string
var start string
var newpart []string

func main() {
	// open file
	f, err := os.Open("table.csv")
	if err != nil {
		log.Fatal(err)
	}

	// remember to close the file at the end of the program
	defer f.Close()

	// read csv values using csv.Reader
	csvReader := csv.NewReader(f)
	data, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	for _, k := range data {
		date = k[0]
		start = k[1]
		newpart = []string{date, start}
		datanew = append(datanew, newpart)
	}
	for _, k := range datanew {
		fmt.Println(k)
	}
	f, err = os.Create("new.csv")
	defer f.Close()

	if err != nil {

		log.Fatalln("failed to open file", err)
	}

	w := csv.NewWriter(f)
	err = w.WriteAll(datanew)
}
