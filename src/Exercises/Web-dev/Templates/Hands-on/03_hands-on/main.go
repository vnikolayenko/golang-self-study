package main

import (
	"html/template"
	"log"
	"os"
)

type hotel struct {
	Name    string
	Address string
	Zip     string
	City    string
	Region  string
}

var Nikolaienko = hotel{
	Name:    "Nikolaienko",
	Address: "Vokzalnaia str.",
	Zip:     "101",
	City:    "Chernihiv",
	Region:  "North",
}
var Vodopian = hotel{
	Name:    "Vodopian",
	Address: "Bydivelnukiv str.",
	Zip:     "103",
	City:    "Dnipro",
	Region:  "South",
}

var hotels = []hotel{Nikolaienko, Vodopian}

var tpl *template.Template

var name = "tpl.gohtml"

func init() {
	tpl = template.Must(template.New("test").ParseFiles(name))
	//tpl = template.Must(template.New("test").Parse("test"))
}

func main() {
	file, err := os.Create("index.html")
	defer file.Close()
	if err != nil {
		log.Fatalf("Couldn't parse file. %v", err)
	}
	err = tpl.ExecuteTemplate(file, name, hotels)
	if err != nil {
		log.Fatalf("Template execution error occured:", err)
	}

}
