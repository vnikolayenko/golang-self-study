package main

import (
	"fmt"
	"html/template"
	"log"
	"os"
)

//var data = "Some data"
//
//func main() {
//	//file, err := os.Create("index.html")
//	//if err != nil {
//	//	fmt.Errorf("Can't create a file")
//	//}
//
//	tpl, err := template.ParseFiles("tpl.gohtml")
//	if err != nil {
//		fmt.Errorf("Can't parse template file")
//	}
//	//err = tpl.Execute(os.Stdout, data)
//	//if err != nil {
//	//	fmt.Errorf("Can't output tpl file")
//	//}
//	file, err := os.Create("index.html")
//	if err != nil {
//		fmt.Errorf("Can't create file")
//	}
//	defer file.Close()
//	err = tpl.Execute(file, data)
//	if err != nil {
//		fmt.Errorf("Can't write to output file")
//	}
//
//}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("template/*"))
}

//var data = []int{1, 2, 3, 4}
var data = struct {
	Firstname string
	Age       int
}{
	"Nikolaienko",
	23,
}

type data1 struct {
	Firstname string
	Age       int
}

var data2 = []data1{
	{"Vodopian", 20}, {"Nikolaienko", 23},
}

func main() {

	file, err := os.Create("index.html")
	if err != nil {
		fmt.Errorf("Can't create file")
	}
	defer file.Close()

	//err = tpl.Execute(file, data)
	//if err != nil {
	//	fmt.Errorf("Can't write to output file")
	//}

	fmt.Println("\n")
	err = tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", data)
	if err != nil {
		log.Fatalf("Can't output tpl file")
	}

	fmt.Println("\n")
	err = tpl.ExecuteTemplate(file, "tpl2.gohtml", data2)
	if err != nil {
		log.Fatalf("Can't output tpl file")
	}
}
