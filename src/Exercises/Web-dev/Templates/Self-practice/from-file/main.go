package main

import (
	"fmt"
	"html/template"
	"log"
	"os"
)

//var data = "Some data"
//
//func main() {
//	//file, err := os.Create("index.html")
//	//if err != nil {
//	//	fmt.Errorf("Can't create a file")
//	//}
//
//	tpl, err := template.ParseFiles("tpl.gohtml")
//	if err != nil {
//		fmt.Errorf("Can't parse template file")
//	}
//	//err = tpl.Execute(os.Stdout, data)
//	//if err != nil {
//	//	fmt.Errorf("Can't output tpl file")
//	//}
//	file, err := os.Create("index.html")
//	if err != nil {
//		fmt.Errorf("Can't create file")
//	}
//	defer file.Close()
//	err = tpl.Execute(file, data)
//	if err != nil {
//		fmt.Errorf("Can't write to output file")
//	}
//
//}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("template/*"))
}

var data = "Some data"

func main() {

	err := tpl.Execute(os.Stdout, data)
	if err != nil {
		fmt.Errorf("Can't output tpl file")
	}
	fmt.Println("\n")
	err = tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", data)
	if err != nil {
		log.Fatalf("Can't output tpl file")
	}
}
