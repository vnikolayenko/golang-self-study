package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

var s = `Loh "pidr" 'pisdiyk'`

func main() {
	//fmt.Println(s)
	s = os.Args[1]
	tpl := `
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
	<title>Hello World!</title>
	</head>
	<body>
	<h1>` + s + `</h1>
	</body>
	</html>
	`
	fmt.Println(tpl)
	file, err := os.Create("index.html")
	if err != nil {
		fmt.Errorf("Can't create file")
	}
	defer file.Close()
	io.Copy(file, strings.NewReader(tpl))
}
