package main

import (
	"bufio"
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net"
	"strings"
)

var tpl *template.Template

func init() {
	//tpl = template.Must(template.ParseFiles(name))
	tpl = template.Must(template.New("os").Parse("<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title></title></head><body><strong>Your operation system:{{.}}</strong></body></html>"))
}

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Can't listen for port 8080 tcp. Err: %v", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Fatalf("Can't accept connection. Err: %v", err)
			continue
		}
		go handle(conn)
	}
}

var b bytes.Buffer

func handle(conn net.Conn) {
	defer conn.Close()
	OS := request(conn)

	err := tpl.ExecuteTemplate(&b, "os", OS)
	if err != nil {
		log.Fatalf("Template execution error occured:", err)
	}
	response := b.String()
	b.Reset()
	respond(conn, response)
}

func request(conn net.Conn) string {
	scanner := bufio.NewScanner(conn)
	OS := ""
	i := 0
	fmt.Printf("New request\n")
	j := 0
	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(ln, i)
		if i == 5 && j == 0 {
			fmt.Printf("i: %v\n j: %v\n", i, j)
			OS = strings.Fields(ln)[3]
			j++
		}
		if i == 3 && j > 0 {
			OS = strings.Fields(ln)[0]
			fmt.Printf("i: %v\n j: %v\n", i, j)
		}
		if ln == "" {
			// headers are done
			break
		}
		i++
	}
	return OS
}

func respond(conn net.Conn, response string) {

	//body := `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title></title></head><body><strong>Your operation system:{{.}}</strong></body></html>`

	fmt.Fprint(conn, "HTTP/1.1 200 OK\r\n")
	fmt.Fprintf(conn, "Content-Length: %d\r\n", len(response))
	fmt.Fprint(conn, "Content-Type: text/html\r\n")
	fmt.Fprint(conn, "\r\n")
	fmt.Fprint(conn, response)

}
