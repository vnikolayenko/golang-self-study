package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("starting-files/templates/index.gohtml"))
}

func main() {
	fs := http.FileServer(http.Dir("starting-files/public"))
	http.Handle("/resources/pics/", http.StripPrefix("/resources", fs))
	http.HandleFunc("/", serve)
	http.ListenAndServe(":8080", nil)

}

func serve(res http.ResponseWriter, req *http.Request) {
	err := tpl.Execute(res, nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
