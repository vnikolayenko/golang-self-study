package main

import (
	"fmt"
)

//func main() {
//	//c := make(chan int)
//	//
//	//go func() {
//	//	c <- 42
//	//}()
//	c := make(chan int, 1)
//	c <- 42
//
//	fmt.Println(<-c)
//}

//func main() {
//	cs := make(chan int)
//	//cs := make(chan<- int)
//	go func() {
//		cs <- 42
//	}()
//	fmt.Println(<-cs)
//
//	fmt.Printf("------\n")
//	fmt.Printf("cs\t%T\n", cs)
//}

//func main() {
//	c := gen()
//	receive(c)
//
//	fmt.Println("about to exit")
//}
//func receive(chanel <-chan int) {
//	for c := range chanel {
//		fmt.Println(c)
//	}
//}
//
//func gen() <-chan int {
//	c := make(chan int)
//	go func() {
//		for i := 0; i < 100; i++ {
//			c <- i
//		}
//		close(c)
//	}()
//
//	return c
//}

//func main() {
//	q := make(chan int)
//	c := gen(q)
//
//	receive(c, q)
//
//	fmt.Println("about to exit")
//}
//func receive(c <-chan int, q <-chan int) {
//	for {
//		select {
//		case <-q:
//			fmt.Println("quit")
//			return
//		case v := <-c:
//			fmt.Println(v)
//		}
//	}
//}
//
//func gen(q chan int) chan int {
//	c := make(chan int)
//	go func() {
//		for i := 0; i < 100; i++ {
//			c <- i
//		}
//		q <- 0
//		close(c)
//	}()
//	return c
//}

//func main() {
//	c := make(chan int)
//	go func() {
//		//for i := 0; i < 10; i++ {
//		//	c <- i
//		//}
//		c <- 25
//		close(c)
//	}()
//
//	//for val := range c {
//	//	v, ok := val
//	//	fmt.Println(val)
//	//	fmt.Println(val, ok)
//	//}
//
//	//val := <-c
//
//	v, ok := <-c
//	fmt.Println(v, ok)
//	v, ok = <-c
//	fmt.Println(v, ok)
//}

//func main() {
//	start := time.Now()
//	elapsed := time.Since(start)
//	c := make(chan int)
//	go func() {
//		for i := 0; i < 10; i++ {
//			c <- i
//		}
//		close(c)
//	}()
//	for v := range c {
//		fmt.Println(v)
//	}
//	fmt.Println(elapsed)
//}

func main() {
	c := make(chan int)
	a := 2
	b := 4

	for i := 0; i < a; i++ {
		go func() {
			for j := 0; j < b; j++ {
				c <- j
			}

		}()
	}

	for k := 0; k < a*b; k++ {
		fmt.Println(k+1, <-c)
	}
}
