package main

import (
	"fmt"
	"sort"
)

//type user struct {
//	First string
//	Age   int
//}
//
//func main() {
//	u1 := user{
//		First: "James",
//		Age:   32,
//	}
//
//	u2 := user{
//		First: "Moneypenny",
//		Age:   27,
//	}
//
//	u3 := user{
//		First: "M",
//		Age:   54,
//	}
//
//	users := []user{u1, u2, u3}
//
//	fmt.Println(users)
//	fmt.Printf("%T\n", users)
//	//var marshal = []uint8{}
//	marshal, err := json.Marshal(users)
//	if err != nil {
//		fmt.Println("ERROR")
//	}
//	//for _, v := range marshal {
//	//	fmt.Println(string(v))
//	//}
//	fmt.Println(string(marshal))
//	fmt.Printf("%T\n", marshal)
//}
//

//func main() {
//	s := `[{"First":"James","Last":"Bond","Age":32,"Sayings":["Shaken, not stirred","Youth is no guarantee of innovation","In his majesty's royal service"]},{"First":"Miss","Last":"Moneypenny","Age":27,"Sayings":["James, it is soo good to see you","Would you like me to take care of that for you, James?","I would really prefer to be a secret agent myself."]},{"First":"M","Last":"Hmmmm","Age":54,"Sayings":["Oh, James. You didn't.","Dear God, what has James done now?","Can someone please tell me where James Bond is?"]}]`
//	fmt.Println(s)
//	fmt.Printf("%T\n", s)
//	bs := []uint8(s)
//	fmt.Println(bs)
//	fmt.Printf("%T\n", bs)
//	person := []Person{}
//	json.Unmarshal(bs, &person)
//	fmt.Println(person)
//	fmt.Printf("%T\n", person)
//
//}
//
//type Person struct {
//	First   string   `json:"First"`
//	Last    string   `json:"Last"`
//	Age     int      `json:"Age"`
//	Sayings []string `json:"Sayings"`
//}

//type user struct {
//	First   string
//	Last    string
//	Age     int
//	Sayings []string
//}
//
//func main() {
//	u1 := user{
//		First: "James",
//		Last:  "Bond",
//		Age:   32,
//		Sayings: []string{
//			"Shaken, not stirred",
//			"Youth is no guarantee of innovation",
//			"In his majesty's royal service",
//		},
//	}
//
//	u2 := user{
//		First: "Miss",
//		Last:  "Moneypenny",
//		Age:   27,
//		Sayings: []string{
//			"James, it is soo good to see you",
//			"Would you like me to take care of that for you, James?",
//			"I would really prefer to be a secret agent myself.",
//		},
//	}
//
//	u3 := user{
//		First: "M",
//		Last:  "Hmmmm",
//		Age:   54,
//		Sayings: []string{
//			"Oh, James. You didn't.",
//			"Dear God, what has James done now?",
//			"Can someone please tell me where James Bond is?",
//		},
//	}
//
//	users := []user{u1, u2, u3}
//
//	fmt.Println(users)
//	json.NewEncoder(os.Stdout).Encode(users)
//
//}

//func main() {
//	xi := []int{5, 8, 2, 43, 17, 987, 14, 12, 21, 1, 4, 2, 3, 93, 13}
//	xs := []string{"random", "rainbow", "delights", "in", "torpedo", "summers", "under", "gallantry", "fragmented", "moons", "across", "magenta"}
//
//	fmt.Println(xi)
//	sort.Ints(xi)
//	// sort xi
//	fmt.Println(xi)
//
//	fmt.Println(xs)
//	sort.Strings(xs)
//	// sort xs
//	fmt.Println(xs)
//}

type user struct {
	First   string
	Last    string
	Age     int
	Sayings []string
}

// ByAge type used for sorting
type ByAge []user

func (a ByAge) Len() int           { return len(a) }
func (a ByAge) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByAge) Less(i, j int) bool { return a[i].Age < a[j].Age }

type ByLast []user

func (a ByLast) Len() int           { return len(a) }
func (a ByLast) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByLast) Less(i, j int) bool { return a[i].Last < a[j].Last }

func main() {
	u1 := user{
		First: "James",
		Last:  "Bond",
		Age:   32,
		Sayings: []string{
			"Shaken, not stirred",
			"Youth is no guarantee of innovation",
			"In his majesty's royal service",
		},
	}

	u2 := user{
		First: "Miss",
		Last:  "Moneypenny",
		Age:   27,
		Sayings: []string{
			"James, it is soo good to see you",
			"Would you like me to take care of that for you, James?",
			"I would really prefer to be a secret agent myself.",
		},
	}

	u3 := user{
		First: "M",
		Last:  "Hmmmm",
		Age:   54,
		Sayings: []string{
			"Oh, James. You didn't.",
			"Dear God, what has James done now?",
			"Can someone please tell me where James Bond is?",
		},
	}

	users := []user{u1, u2, u3}

	fmt.Println(users)
	fmt.Println("Sort by Age:")
	sort.Sort(ByAge(users))
	fmt.Println(users)
	fmt.Println("Sort by Last:")
	sort.Sort(ByLast(users))
	fmt.Println(users)
	fmt.Println("Sort Sayings:")
	for _, v := range users {
		sort.Strings(v.Sayings)
	}
	fmt.Println(users)

	// your code goes here

}
