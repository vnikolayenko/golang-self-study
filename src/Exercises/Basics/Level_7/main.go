package main

import "fmt"

func main() {
	v := 4
	fmt.Println(&v)
	person := person{
		firstname: "Anon",
		lastname:  "Anon",
		age:       0,
	}
	fmt.Println(person)
	callMe(&person)
	fmt.Println(person)
}

type person struct {
	firstname string
	lastname  string
	age       int
}

func callMe(p *person) {
	(*p).age = 22
	(*p).lastname = "Vlad"
	(*p).firstname = "Nikolaienko"
}
