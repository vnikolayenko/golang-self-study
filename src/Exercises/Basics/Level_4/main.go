package main

import "fmt"

func main() {
	//x := []int{0, 0, 0, 0}
	//y := []int{1, 2, 3, 4, 5}
	//z := []int{6, 7, 8, 9, 10}
	//x[3] = 5
	//fmt.Println(x)
	//fmt.Println(y)
	//fmt.Println("\n\n\n\n")
	//x = append(x, y...)
	//fmt.Println(x)
	//xp := [][]int{x, y, z}
	//fmt.Println(len(xp))
	//for i, v := range xp {
	//	fmt.Println(v)
	//	for _, b := range xp[i] {
	//		fmt.Println(b)
	//	}
	//}
	//x := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	//y := []int{13, 14, 15}
	//z := x[1:3]
	//x = append(x[:3], x[4:]...)
	//y = append(y, z...)
	//
	//arr := make([]string, 10, 100)
	//arr = append(arr, "Chernihiv", "Lviv")
	//fmt.Println(arr)
	//fmt.Println(len(arr))
	//fmt.Println(cap(arr))
	//

	//fmt.Println(y)
	//x[1] = 1
	//x[2] = 2
	//x[3] = 3
	//x[4] = 4
	//x[0] = 0
	//for _, v := range x {
	//	fmt.Println(v)
	//}
	//fmt.Printf("%T\n", x)

	//y := []int{}
	//fmt.Println(x[1:3])

	x := map[string][]string{
		"Anon": []string{"0", "Haccker"},
	}
	y := []string{"23", "DevOps Engineer"}
	z := []string{"22", "Software Engineer"}
	x["Vlad"] = y
	x["Maks"] = z
	delete(x, "Anon")
	for k, v := range x {
		fmt.Println(k)
		for i, val := range v {
			fmt.Println(i, val)
		}
	}
}
