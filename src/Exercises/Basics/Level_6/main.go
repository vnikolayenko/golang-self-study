package main

import (
	"fmt"
	"math"
)

//func main() {
//	defer hello()
//	a := []int{1, 2, 3, 4, 5}
//	fmt.Println(foo(a...))
//	fmt.Println(bar(a))
//}
//func hello() {
//	fmt.Println("Hello World")
//}
//func foo(a ...int) int {
//	sum := 0
//	for _, v := range a {
//		sum += v
//	}
//	return sum
//}
//func bar(a []int) int {
//	sum := 0
//	for _, v := range a {
//		sum += v
//	}
//	return sum
//}

//func main() {
//	p := person{
//		first: "Vlad",
//		last:  "Nikolaienko",
//		age:   23,
//	}
//	p.speak()
//}
//
//type person struct {
//	first string
//	last  string
//	age   int
//}
//
//func (p person) speak() {
//	fmt.Println("My name is", p.first, p.last, ".\nI am the", p.age, "years old")
//}

func main() {
	a := square{
		side: 6,
	}
	r := circle{
		radius: 3,
	}
	//fmt.Println(a)
	//fmt.Println(r)

	info(a)
	info(r)
	//func() {
	//	fmt.Println("Hello World")
	//}()
	//b := func() int {
	//	fmt.Println("Hello World")
	//	return 2
	//}
	//fmt.Println(b())

	c := call(1)
	fmt.Println(c())
	c1 := call1(1)
	fmt.Println(c1)
	fmt.Println("Closure:")
	cl := closure()
	fmt.Println(cl())
	fmt.Println(cl())

}

type square struct {
	side float64
}

type circle struct {
	radius float64
}

func (s square) area() float64 {
	return s.side * s.side
}

func (c circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

type shape interface {
	area() float64
}

func info(a shape) {
	fmt.Println(a)
	area := a.area()
	fmt.Println(area)
}
func sum(a int, b int) int {
	return a + b
}
func call(b int) func() int {
	a := 1
	return func() int {
		return a + b
	}
}
func call1(b int) int {
	a := 1
	return sum(a, b)
}

func callback(c func(a int, b int) int, a int, b int) int {
	result := c(a, b) + 1
	return result
}
func closure() func() int {
	a := 0
	return func() int {
		a++
		return a
	}
}
