package main

import (
	"fmt"
	"runtime"
)

//var wg sync.WaitGroup
//
//func main() {
//	fmt.Println("Main")
//	wg.Add(2)
//	go routine_1()
//	go routine_2()
//	wg.Wait()
//}
//
//func routine_1() {
//	fmt.Println("First")
//	wg.Done()
//}
//
//func routine_2() {
//	fmt.Println("Second")
//	wg.Done()
//}
//

//type person struct {
//	First string
//	Last  string
//	age   int8
//}
//
//func (p *person) speak() {
//	fmt.Printf("My name %s %s. I am %d years old\n", p.First, p.Last, p.age)
//}
//
//type human interface {
//	speak()
//}
//
//func main() {
//	person := person{
//		First: "Vlad",
//		Last:  "Nikolaienko",
//		age:   23,
//	}
//	SaySomething(&person)
//	//SaySomething(person)
//}
//
//func SaySomething(h human) {
//	h.speak()
//}
//

//func main() {
//
//	fmt.Println("CPU", runtime.NumCPU())
//	fmt.Println("Goroutin", runtime.NumGoroutine())
//
//	var wg sync.WaitGroup
//	//var mute sync.Mutex
//	var icrement int64
//	wg.Add(20)
//	for i := 0; i < 20; i++ {
//		go func() {
//			//mute.Lock()
//			atomic.AddInt64(&icrement, 1)
//			//b := icrement
//			//runtime.Gosched()
//			//b++
//			//icrement = b
//			fmt.Println("increment:", icrement)
//			//mute.Unlock()
//			//fmt.Println("Goroutin", runtime.NumGoroutine())
//			wg.Done()
//		}()
//	}
//
//	//fmt.Println("CPU", runtime.NumCPU())
//	//fmt.Println("Goroutin", runtime.NumGoroutine())
//
//	wg.Wait()
//	fmt.Println("Final increment:", icrement)
//	//fmt.Println("Goroutin", runtime.NumGoroutine())
//}
//

func main() {
	arch := runtime.GOARCH
	os := runtime.GOOS
	fmt.Println("OS:\t", os, "\nARCH:\t", arch)
}
