package main

import "fmt"

//func main() {
//	x := 43
//	y := "James Bond"
//	z := true
//	fmt.Println(x)
//	fmt.Println(y)
//	fmt.Println(z)
//	fmt.Println(x, "\n", y, "\n", z, "\n")
//}

//var x int = 42
//var y string = "James Bond"
//var z bool = true

//func main() {
//	//fmt.Printf("%T\n", x)
//	//fmt.Printf("%T\n", y)
//	//fmt.Printf("%T\n", z)
//	//fmt.Println(x, y, z)
//	s := fmt.Sprintf("%v\t%v\t%v", x, y, z)
//	fmt.Println(s)
//
//}

//type own int
//
//var x own
//var y int
//var z bool

//func main() {
//	//fmt.Println(x)
//	//fmt.Printf("%T\n", x)
//	//x = 42
//	//y = int(x)
//	//fmt.Println(x)
//	//fmt.Println(z)
//	z := "Hello"
//	z = "Hello World"
//	fmt.Println(z)
//}
func printChars(s string) {
	fmt.Printf("Characters: ")
	for i := 0; i < len(s); i++ {
		fmt.Printf("%c ", s[i])
	}
}

func main() {
	name := "Hello World"
	fmt.Printf("String: %s\n", name)
	printChars(name)
	fmt.Printf("\n")
	//printBytes(name)
}
