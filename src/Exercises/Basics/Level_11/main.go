package main

import (
	"fmt"
	"log"
)

//type person struct {
//	First   string
//	Last    string
//	Sayings []string
//}
//
//func main() {
//	p1 := person{
//		First:   "James",
//		Last:    "Bond",
//		Sayings: []string{"Shaken, not stirred", "Any last wishes?", "Never say never"},
//	}
//
//	bs, err := json.Marshal(p1)
//	if err != nil {
//		fmt.Println(err)
//	}
//	fmt.Println(string(bs))
//
//}

//type person struct {
//	First   string
//	Last    string
//	Sayings []string
//}
//
//func main() {
//	p1 := person{
//		First:   "James",
//		Last:    "Bond",
//		Sayings: []string{"Shaken, not stirred", "Any last wishes?", "Never say never"},
//	}
//
//	bs, err := toJSON(p1)
//	if err != nil {
//		fmt.Println(err)
//		//log.Println(err)
//		return
//		//log.Fatalln(err)
//
//	}
//	fmt.Println(string(bs))
//
//}
//
//// toJSON needs to return an error also
//func toJSON(a interface{}) ([]byte, error) {
//	bs, err := json.Marshal(a)
//	if err != nil {
//		return []byte{}, fmt.Errorf("An error occured: %v", err)
//	}
//	return bs, err
//}
//

//type customErr struct {
//	err string
//}
//
//func (ce customErr) Error() string {
//	return fmt.Sprintf("Custom error: %v", ce.err)
//}
//
//func main() {
//	ce := customErr{
//		err: "Error message",
//	}
//	foo(ce)
//
//}
//
//func foo(e error) {
//	fmt.Println(e)
//}

type sqrtError struct {
	lat  string
	long string
	err  error
}

func (se sqrtError) Error() string {
	return fmt.Sprintf("math error: %v %v %v", se.lat, se.long, se.err)
}

func main() {
	_, err := sqrt(-10.23)
	if err != nil {
		log.Println(err)
	}
}

func sqrt(f float64) (float64, error) {
	se := sqrtError{
		lat:  "50.2289 N",
		long: "99.4656 W",
		err:  fmt.Errorf("The value is negative: %v", f),
	}
	if f < 0 {
		return 0, se
	}
	return 42, nil
}
