package main

import "fmt"

func main() {
	defer foo()
	fmt.Println("1")
	fmt.Println("2")
	//foo()
}

func foo() {
	fmt.Println("test")
}
