package main

import "fmt"

func main() {
	type person struct {
		firstname string
		lastname  string
		flavor    []string
	}
	//var person1 = person{
	//	firstname: "Lol1",
	//	lastname:  "kek",
	//	flavor:    []string{"lol1", "kek1"},
	//}
	//person2 := person{
	//	firstname: "Lol2",
	//	lastname:  "kek",
	//	flavor:    []string{"lol2", "kek2"},
	//}
	//fmt.Println(person1.firstname, person2.flavor[:])
	//for i, v := range person2.flavor {
	//	fmt.Println(i, v)
	//}

	//m := map[string]person{
	//	person1.firstname: person1,
	//	person2.firstname: person2,
	//}
	//for k, v := range m {
	//	for _, val := range v.flavor {
	//		fmt.Println(k, val)
	//	}

	//}
	type vehicle struct {
		doors int
		color string
	}
	type sedan struct {
		vehicle
		luxury bool
	}
	type truck struct {
		vehicle
		fourWheel bool
	}
	car1 := truck{
		vehicle: vehicle{
			doors: 4,
			color: "red",
		},
		fourWheel: false,
	}
	car2 := sedan{
		vehicle: vehicle{
			doors: 2,
			color: "green",
		},
		luxury: true,
	}
	fmt.Println(car1)
	fmt.Println(car1.doors, car1.color, car1.fourWheel)

	fmt.Println(car2.vehicle.color, car2.vehicle.doors, car2.luxury)

	car3 := struct {
		vehicle
		typ string
	}{
		vehicle: vehicle{
			doors: 3,
			color: "blue",
		},
		typ: "unknown",
	}
	fmt.Println(car3)
	cars := map[string][]vehicle{
		"variaty2": {
			{
				doors: 7,
				color: "pink",
			},
			{
				doors: 6,
				color: "purple",
			},
		},
	}
	for k, v := range cars {
		fmt.Println(k, v)
	}
}
