package hello

import (
	"hello/hello_world"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	want := "Hello, world."
	if got := hello_world.HelloWorld(); got != want {
		t.Errorf("Hello() = %q, want %q", got, want)
	}
}
